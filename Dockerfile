FROM docker.io/p3terx/darkhttpd:1.16

WORKDIR /public

COPY . /public

EXPOSE 80

CMD ["/public"]
